﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace DiscordBot
{
    public class Api
    {
        public delegate void EventHandler(object arg,object arg2);
        public event EventHandler SendMessage;
        public event EventHandler MoveUser;
        public event EventHandler RoleAdded;
        public event EventHandler RoleRemoved;
        public event EventHandler GetRoles;
        public Api()
        {
            
        }
        
        public  void GetCommand(string postData)
        {
            var posts = HttpUtility.ParseQueryString(postData);

            if (posts.AllKeys.Contains("cmd"))
            {
                if (posts.Get("cmd").Contains("msg"))
                {
                    var user = ulong.Parse(posts.Get("discordid"));
                    var message = posts.Get("message");
                    SendMessage?.Invoke(user,message);
                }
                if (posts.Get("cmd").Contains("mv"))
                {
                  
                    var user = ulong.Parse(posts.Get("discordid"));
                    var channel = ulong.Parse(posts.Get("channelid"));
                    MoveUser?.Invoke(user,channel);
                }
                if (posts.Get("cmd").Contains("arole"))
                {
                  
                    var user = ulong.Parse(posts.Get("discordid"));
                    var roleid = ulong.Parse(posts.Get("roleid"));
                    RoleAdded?.Invoke(user,roleid);
                }
                if (posts.Get("cmd").Contains("rrole"))
                {
                  
                    var user = ulong.Parse(posts.Get("discordid"));
                    var roleid = ulong.Parse(posts.Get("roleid"));
                    RoleRemoved?.Invoke(user,roleid);
                }
                if (posts.Get("cmd").Contains("getroles"))
                {
                   GetRoles?.Invoke("","");
                }
            }

        }
    }
}