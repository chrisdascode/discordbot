﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;


namespace DiscordBot
{
    public static class Loader
    {
        private static DiscordBot _db;
        private const string Password = "password=coresails4321";
        private static string _server;
        private const bool IsDev = false;

        public static void Main(string[] args)
        {
            _server = IsDev ? "http://localhost:9090/index/" : "http://159.65.176.141:9090/index/";
            _db = new DiscordBot();
            _db.Connected += client => { Console.WriteLine("Connected"); };
            _db.Received += (client, message) =>
            {
                Console.WriteLine($"[MSG] [Channel: {message.Channel.Name}] {message.CreatedAt.ToString()} - {message.Author.Username}: {message.Content}");
            };
            _db.LogEvent += message =>
            {
                Console.WriteLine($"[Log]: [{message.Severity} ]  ({message.Message}) ");
            };

            using (var ws = new WebServer(SendResponse, _server))
            {
                ws.Run();
                _db.Start();
                Console.ReadLine();
                ws.Stop();
            }
    
        }

        private static string GetTemplate()
        {
            var path = Environment.CurrentDirectory + "/base.html";
            var templateData = File.ReadAllText(path);
            return templateData ?? "";
        }

        private static string GetBody()
        {
            var path = Environment.CurrentDirectory + "/body.html";
            var bodyData = File.ReadAllText(path);
            return bodyData ?? "";
        }

        private static string SendResponse(HttpListenerRequest request)
        {
            if (!request.RawUrl.Contains("index")) return "404";
            var template = GetTemplate();
            var body = GetBody();
            template = template.Replace("{{TITLE}}", "DiscordBot Homepage");
            template = template.Replace("{{BODY}}", body);
            using (var rbody = request.InputStream) // here we have data
            {
                using (var reader = new StreamReader(rbody, request.ContentEncoding))
                {
                    var postData = reader.ReadToEnd();
                    if (!postData.Contains(Password)) return template;
                    var api = new Api();
                    api.SendMessage += (user, message) => { _db.SendMessage((ulong) user, (string) message); };
                    api.MoveUser += (user, channel) => { _db.ChangeChannels((ulong) user, (ulong) channel); };
                    api.RoleAdded += (user, role) => { _db.AddRole((ulong) user, (ulong) role); };
                    api.RoleRemoved += (user, role) => { _db.RemoveRole((ulong) user, (ulong) role); };
                    api.GetRoles += (x, y) =>
                    {
                        var roles = _db.GetRoles();
                        foreach (var r in roles)
                        {
                            Console.WriteLine($"[Role]: {r.Name} - {r.Id}");
                        }
                    };
                    api.GetCommand(postData);
                }
            }
           

            return template;
        }
    }
}