﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace DiscordBot
{
    public class DiscordBot : IDisposable
    {
        public delegate void ConnectionHandler(DiscordSocketClient client);

        public delegate void Log(LogMessage message);

        public delegate void MessageHandler(DiscordSocketClient client, SocketMessage message);

        public delegate void VoiceHandler(SocketUser user, SocketVoiceState state, SocketVoiceState state2);

        private const string Token = "//removed";
        private const ulong GuildId = 284869580105449472;
        private const ulong RoleId = 351609329780129792;
        private DiscordSocketClient _client;


        public void Dispose()
        {
           
        }

        public async void SendMessage(ulong user,string message)
        {
            var u = _client.GetUser(user);
            await u.SendMessageAsync(message);
        }

        public async void ChangeChannels(ulong user, ulong channel)
        {
            var guild = _client.GetGuild(GuildId);
            var u = guild.GetUser(user);
            await u.ModifyAsync(x => x.ChannelId = channel);
            var chanName = guild.GetChannel(channel).Name;
            await u.SendMessageAsync($"You have been moved too channel {chanName}");
        }

        public event MessageHandler Received;
        public event ConnectionHandler Connected;
        public event VoiceHandler VoiceStateUpdated;
        public event Log LogEvent;

        public void Start()
        {
            MainAsync().GetAwaiter().GetResult();
        }

        private async Task MainAsync()
        {
            _client = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Info
            });

            _client.Log += message =>
            {
                LogEvent?.DynamicInvoke(message);
                return Task.CompletedTask;
            };


            await _client.LoginAsync(TokenType.Bot, Token);
            await _client.StartAsync();
            SocketGuild guild = null;
            _client.Ready += () =>
            {
                Connected?.DynamicInvoke(_client);
                guild = _client.GetGuild(GuildId);
                return Task.CompletedTask;
            };


            _client.MessageReceived += message =>
            {
                Received?.DynamicInvoke(_client, message);
                return Task.CompletedTask;
            };
            _client.UserVoiceStateUpdated += (user, coming, going) =>
            {
                VoiceStateUpdated?.DynamicInvoke(user, coming, going);
                var role = GetRoleById(guild, RoleId);
                var sGuildUser = guild.GetUser(user.Id);
                if (going.ToString().StartsWith("Team"))
                    sGuildUser.AddRoleAsync(role);
                else
                    sGuildUser.RemoveRoleAsync(role);


                return Task.CompletedTask;
            };

            //Block this task until the program is closed.
            await Task.Delay(-1);
        }

        private static IRole GetRoleById(SocketGuild guild, ulong roleId)
        {
            IRole irole = null;
            foreach (var role in guild.Roles)
                if (role.Id == roleId)
                    irole = role;

            return irole;
        }
    }
}